%{
#include <cstdio>
#include <string>

#define CONCAT(strings) strdup(((std::string) strings).c_str())

union YYSTYPE;
extern "C" YYSTYPE yylval;
extern "C" int yylex();
static void yyerror(const char *msg);
static const char *res;
%}

%union
{
    char c;
    int d;
    const char *s;
}

%token<d> BOOL
%token<s> CHAR INT FLOAT STRING ID
%token OP OP_EQ
%token KW_BOOL KW_CHAR KW_SHORT KW_INT KW_DOUBLE
%token KW_IF KW_ELSE KW_FOR KW_WHILE
%token FUNC_PRINTF FUNC_PUTS FUNC_SCANF

%type<s> CppProgram ScopeStatement BlockStatement StatementList Statement
%type<s> VarDeclStatement IfStatement ForStatement WhileStatement
%type<s> ExpressionStatement IOCallStatement
%type<s> Expression UnaryExpression BasicType

%%


CppProgram:
	Statement  { res = $$ = $1; }
	| StatementList  { res = $$ = $1; }
;

ScopeStatement:
	Statement  { $$ = $1; }
	| BlockStatement  { $$ = $1; }
;

BlockStatement:
	'{' '}'  { $$ = "Begin\nEnd\n"; }
	| '{' StatementList '}'  { $$ = CONCAT("Begin\n" + $2 + "End\n"); }
;

StatementList:
	Statement  { $$ = $1; }
	| Statement StatementList  { $$ = CONCAT($1 + $2); }
;

Statement :
	VarDeclStatement       { $$ = $1; }
	| IfStatement          { $$ = $1; }
	| ForStatement         { $$ = $1; }
	| WhileStatement       { $$ = $1; }
	| ExpressionStatement  { $$ = $1; }
	| IOCallStatement      { $$ = $1; }
	| ';' { $$ = "\n"; }
;

VarDeclStatement :
	BasicType ID ';'
	{ $$ = CONCAT("Dim " + $2 + " As " + $1 + "\n"); }

	| BasicType ID '=' Expression ';'
	{ $$ = CONCAT("Dim " + $2 + " As " + $1 + "\n" + $2 + " := " + $4 + "\n"); }
;

IfStatement:
	KW_IF '(' Expression ')' ScopeStatement
	{ $$ = CONCAT("IF " + $3 + " THEN\n" + $5 + "End If\n"); }

	| KW_IF '(' Expression ')' ScopeStatement KW_ELSE ScopeStatement
	{ $$ = CONCAT("If " + $3 + " Then\n" + $5 + "Else\n" + $7 + "End If\n"); }
;

ForStatement:
	KW_FOR '(' ID '=' Expression ';' ID '<' Expression ';' ID '=' ID '+' Expression ')' ScopeStatement
	{
		$$ = CONCAT("For " + $3 + " = " + $5 +
			" To " + $9 +" Step " + $15 +"\n" +
			$17 + "Next i\n");
	}
;

WhileStatement:
	KW_WHILE '(' Expression ')' ScopeStatement
	{ $$ = CONCAT("While " + $3 + " do\n" + $5 + "End While\n"); }
;

ExpressionStatement:
	Expression ';'
	{ $$ = CONCAT($1 + ";\n"); }
;

IOCallStatement:
	FUNC_PRINTF '(' STRING ',' Expression ')' ';'
	{ $$ = CONCAT("PRINT " + $5 + "\n"); }

	| FUNC_PUTS '(' STRING ')' ';'
	{ $$ = CONCAT("PRINT " + $3 + "\n"); }

	| FUNC_SCANF '(' STRING ',' '&' Expression ')' ';'
	{ $$ = CONCAT("INPUT " + $6 + "$\n"); }
;

Expression:
	UnaryExpression  { $$ = $1; }
	| UnaryExpression '+' Expression  { $$ = CONCAT($1 + " + " + $3); }
	| UnaryExpression '-' Expression  { $$ = CONCAT($1 + " - " + $3); }
	| UnaryExpression '*' Expression  { $$ = CONCAT($1 + " * " + $3); }
	| UnaryExpression '/' Expression  { $$ = CONCAT($1 + " / " + $3); }
	| UnaryExpression '<' Expression  { $$ = CONCAT($1 + " < " + $3); }
	| UnaryExpression '>' Expression  { $$ = CONCAT($1 + " > " + $3); }
	| UnaryExpression OP_EQ Expression  { $$ = CONCAT($1 + " = " + $3); }
	| ID              '=' Expression  { $$ = CONCAT($1 + " := " + $3); }
	| '(' Expression ')'  { $$ = CONCAT("(" + $2 + ")"); }
;

UnaryExpression:
	'-' UnaryExpression    { $$ = CONCAT("-" + $2); }
	| '+' UnaryExpression  { $$ = CONCAT("+" + $2); }
	| BOOL    { $$ = $1 ? "True" : "False"; }
	| CHAR    { $$ = $1; }
	| INT     { $$ = $1; }
	| FLOAT   { $$ = $1; }
	| STRING  { $$ = $1; }
	| ID      { $$ = $1; }
;

BasicType:
	KW_BOOL      { $$ = "Boolean"; }
	| KW_CHAR    { $$ = "Char"; }
	| KW_SHORT   { $$ = "Integer"; }
	| KW_INT     { $$ = "LongInt"; }
	| KW_DOUBLE  { $$ = "Double"; }
;

%%

void yyerror(const char *msg)
{
  fprintf(stderr, "Parsing error: %s\n", msg);
}

int main()
{
  /*yydebug = 1;*/
  yyparse();
  puts(res);
}
