%{
// #include <cstring>
#include "string.h"
#include "CppToPasConv.tab.h"

#define LEXING_D(t) //printf("lexing: %s %s\n", #t, yytext)
%}


bool   "true"|"false"
char   '(\\.|[^\\'])+'
int    [0-9]+
float  [0-9]*\.[0-9]+|[0-9]+\.[0-9]*
string \"(\\.|[^\\"])*\"
id     [_a-zA-Z][_a-zA-Z0-9]*
op     [+\-*/<>,=&;(){}]
ws     [ \t\r\n]+

%option yylineno
%option noyywrap

%%

{bool} { LEXING_D(bool); yylval.d = yytext[0] == 't'; return BOOL; }
{char} { LEXING_D(bool); yylval.s = strdup(yytext); return CHAR; }
{int} { LEXING_D(int); yylval.s = strdup(yytext); return INT; }
{float} { LEXING_D(int); yylval.s = strdup(yytext); return FLOAT; }
{string} { LEXING_D(string); yylval.s = strdup(yytext); return STRING; }

bool    return KW_BOOL;
char    return KW_CHAR;
short   return KW_SHORT;
int     return KW_INT;
double  return KW_DOUBLE;

if     return KW_IF;
else   return KW_ELSE;
for    return KW_FOR;
while  return KW_WHILE;

printf  return FUNC_PRINTF;
puts    return FUNC_PUTS;
scanf   return FUNC_SCANF;

{id} { LEXING_D(id); yylval.s = strdup(yytext); return ID; }

==     return OP_EQ;
{op} { LEXING_D(op); return yylval.c = yytext[0]; }

{ws} { }

%%
